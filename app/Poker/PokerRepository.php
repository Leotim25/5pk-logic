<?php 

namespace App\Poker;
abstract class PokerRepository implements PokerInterface{
    
    /**
     *s-spades黑桃h-hearts紅桃d-diamonds方塊c-clubs梅花 
     **/
    protected $suit= array('A','B','C','D');
    protected $ranks = array('1','2','3','4','5','6','7','8','9','A','B','D','E');

    protected static $cards = [
        //数字，花色
        [1, 4], [1, 3], [1, 2], [1, 1],
        [2, 4], [2, 3], [2, 2], [2, 1],
        [3, 4], [3, 3], [3, 2], [3, 1],
        [4, 4], [4, 3], [4, 2], [4, 1],
        [5, 4], [5, 3], [5, 2], [5, 1],
        [6, 4], [6, 3], [6, 2], [6, 1],
        [7, 4], [7, 3], [7, 2], [7, 1],
        [8, 4], [8, 3], [8, 2], [8, 1],
        [9, 4], [9, 3], [9, 2], [9, 1],
        [10, 4], [10, 3], [10, 2], [10, 1],
        [11, 4], [11, 3], [11, 2], [11, 1],
        [12, 4], [12, 3], [12, 2], [12, 1],
        [13, 4], [13, 3], [13, 2], [13, 1],
    ];
    
    protected $nowLeftCards;
    public function __construct()
    {
        $this->nowLeftCards = self::$cards;
        //洗牌
        shuffle($this->nowLeftCards);
    }
    
    
    
    /**
     * 获取牌面数字
     * @param $card
     * @return mixed
     */
    protected function getCardNumber($card)
    {
        return $card[0];
    }
    /**
     * 获取所有牌的牌面
     * @param $cards
     * @return array
     */
    protected function getCardNumbers($cards)
    {
        $numbers = [];
        foreach ($cards as $card) {
            $numbers[] = $this->getCardNumber($card);
        }
        return $numbers;
    }
    /**
     * 获取花色
     * @param $card
     * @return mixed
     */
    protected function getCardColor($card)
    {
        return $card[1];
    }
    /**
     * 获取所有牌的花色
     * @param $cards
     * @return array
     */
    protected function getCardColors($cards)
    {
        $colors = [];
        foreach ($cards as $card) {
            $colors[] = $this->getCardColor($card);
        }
        return $colors;
    }
    
    /**
     * 判断牌型
     * @param array $cards 从大到小排序过的牌
     * @return int 10:皇家同花顺 | 9:同花顺 | 8:四条 | 7:葫芦 | 6:同花 | 5:顺子 | 4:三条 | 3:两对 | 2:一对 | 1:高牌
     */
    public function judge(array $cards)
    {
        //print_R($cards);
        $numbers = $this->getCardNumbers($cards);
        foreach ($numbers as $key => $number) {
            if($number == 1){
                $numbers[$key] = 14;
            }
        }
        rsort($numbers);
        $color = $this->getCardColors($cards);
        //皇家同花顺
        if ($this->checkStraight($numbers) && $this->checkFlush($color) && $numbers == [14, 13, 12, 11, 10]) {
            return 10;
        }
        //同花顺
        if ($this->checkStraight($numbers) && $this->checkFlush($color)) {
            return 8;
        }
        //四条
        if ($this->checkSame($numbers, 4) == 1) {
            return 7;
        }
        //葫芦, 有1个3条 和 2个2条
        if ($this->checkSame($numbers, 3) == 1 && $this->checkSame($numbers, 2) == 2) {
            return 6;
        }
        //同化
        if ($this->checkFlush($color)) {
            return 5;
        }
        //顺子
        if ($this->checkStraight($numbers)) {
            return 4;
        }
        //三条
        if ($this->checkSame($numbers, 3) == 1) {
            return 3;
        }
        //两对
        if ($this->checkSame($numbers, 2) == 2) {
            return 2;
        }
        //一对
        if ($this->checkSame($numbers, 2) == 1) {
            return 1;
        }
        //高牌
        return 0;
    }
    
    
    /**
     * 检查是否相同牌面，可判断 4条，3条，2条
     * @param array $numbers
     * @param int   $same
     * @param array $sameCounts 相同的结果
     * @return int
     */
    public function checkSame(array $numbers, $same = 4, array &$sameCounts = [])
    {
        // 桶方法
        foreach ($numbers as $number) {
            if (!isset($sameCounts[$number])) {
                $sameCounts[$number] = 1;
            } else {
                $sameCounts[$number]++;
            }
        }
        $sameNumber = 0;
        foreach ($sameCounts as $key => $sameCount) {
            if (($ceil = (int)($sameCount / $same)) > 0) {
                $sameNumber += $ceil;
            }
        }
        return $sameNumber;
    }
    /**
     * 检查是不是 顺子
     * @param $numbers
     * @return bool
     */
    public function checkStraight($numbers)
    {
        
        $straights = array(
            array(
              1=>14,2,3,4,5  
            ),
            array(
                1=>2,3,4,5,6
            ),
            array(
                1=>3,4,5,6,7
            ),
            array(
                1=>4,5,6,7,8
            ),
            array(
                1=>5,6,7,8,9
            ),
            array(
                1=>6,7,8,9,10
            ),
            array(
                1=>7,8,9,10,11
            ),
            array(
                1=>8,9,10,11,12
            ),
            array(
                1=>9,10,11,12,13
            ),
            array(
                1=>10,11,12,13,14
            )
        );
        foreach ($straights as $straight) {
            foreach ($numbers as $number) {
                
                $key = array_search($number,$straight);
                if($key){
                    unset($straight[$key]);
                }
            }
            if(empty($straight)){
                return true;
            }
        }
        return false;
    }
    /**
     * 检查是不是 同花
     * @param $color
     * @return bool
     */
    public function checkFlush($color)
    {
        return count(array_unique($color)) == 1;
    }

    
}