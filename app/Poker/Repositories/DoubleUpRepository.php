<?php 
namespace App\Poker\Repositories;
use App\Poker\PokerRepository;

class DoubleUpRepository extends PokerRepository {
    
    function __construct(){
        parent::__construct();
        $temp = array();
        foreach ($this->ranks as $v){
            if($v == "A"){
                $temp[10]=$v;
            }else if($v == "B"){
                $temp[11]=$v;
            }else if($v == "D"){
                $temp[12]=$v;
            }else if($v == "E"){
                $temp[13]=$v;
            }else{
                $temp[$v]=$v;
            }
        }
        $temp[99] = "F";
        ksort($temp);
        $this->ranks = $temp;
        $temp2 = array();
        //'S','H','D','C'
        foreach ($this->suit as $v){
            if($v == "A"){
                $temp2[4]=$v;
            }else if($v == "B"){
                $temp2[3]=$v;
            }else if($v == "C"){
                $temp2[2]=$v;
            }else if($v == "D"){
                $temp2[1]=$v;
            }
        }
        $this->suit = $temp2;
    }
    public function cards2array($cards){
        $out = array();
        foreach($cards as $k=>$data){
            $suit = $data[0];
            $ranks = $data[1];
            if(isset($data[2])){
                $ranks = $data[1].$data[2];
            }
            $out[$k] = array(array_search($ranks, $this->ranks) , array_search($suit, $this->suit));
        }
        return $out;
    }
    public function array2cards($array){
        $out = array();
        foreach($array as $k=>$data){
            $suit = $data[1];
            $ranks = $data[0];
            $out[$k] = $this->suit[$suit].$this->ranks[$ranks];
        }
        return $out;
    }
    
    public function show($id){
        $out = array();
        $db = new DoubleUp();
        $res = $db->query()->where(array('id'=>$id))->get();
        if($res){
            $res = $res->toArray();
            if(!empty($res)){
                $out = $res;
                $res2 = $db->query()->where(array('parent_id'=>$id))->orderBy('id', 'desc')->get();
                //print_R($id);
                if($res2){
                    $res2 = $res2->toArray();
                    if(!empty($res2)){
                        $out[0]['child'] = $res2;
                        return $out;
                    }
                }
                return $out;
            }
        }
        return false;
    }
    
    
    public function randomRank($j=5,$palyers = array('A','B','C','D'))
    {
        $all = $this->array2cards($this->nowLeftCards);
        $out = array();
        foreach($palyers as $index => $name){
            for($i=0;$i<$j;$i++){
                $out[$name][] = array_pop($all);
            }
        }
        $out['discard'] = $all;
        return $out;
    }
    public function step1($id = 0){
        $out = array();
        $start = $this->randomRank(5,array('all'));
        $card = $start['all'];
        $A = $start['all'][0];
        unset($start['all'][0]);
        $input = array();
        $input['A'] = json_encode($A);
        $input['card'] = json_encode($card);
        if($id == 0){
            $id = $this->insert(0,$input['card']);
        }else{
            $id = $this->insert($id,$input['card']);
        }
        $out['pc'] = array($A);
        $out['id'] = $id;
        return $out;
    }
    private function insert($id,$card){
        if($card !=""){
            $db = new DoubleUp();
            $db->player_card = "";
            $db->card = $card;
            $db->parent_id = $id;
            $db->status = 0;
            $db->save();
            return $db->id;
        }
        return false;
        
    }
    private function aBigger($A ,$B){
        //0尚未開始玩1平2小3大
        if(is_array($A) && is_array($A) && $A !="" && $B !=""){
            if($A[0] == 1){
                $A[0] = 14;
            }
            if($B[0] == 1){
                $B[0] = 14;
            }
            if(intval($A[0]) > intval($B[0])){
                return 3;
            }
            if(intval($A[0]) == intval($B[0])){
                return 2;
            }
            if(intval($A[0]) < intval($B[0])){
                return 1;
            }
        }
        return 0;
    }
    
    public function step2($id,$site){
        $out = array();
        $db = new DoubleUp();
        $res = $db->query()->where(array('id'=>$id))->get();
        if(empty($res->toArray()) || !isset($res->toArray()[0])){
            $out['status'] = false;
            return $out;
        }
        if(!in_array($site , array(1,2,3,4))){
            $out['status'] = false;
            return $out;
        }
        $data = $res->toArray()[0];
        $cards = json_decode($data['card'],true);
        $cards = $this->cards2array($cards);
        $A = $cards[$site];
        $PC = $cards[0];
        if(intval($data['status']) == 0){
            $status = $this->aBigger($A,$PC);
            $A = $this->array2cards(array($A));
            $A = $A[0];
            $res = $db->query()->where(array('id'=>$id))->update(array('status'=>$status , 'player_card'=>'["'.$A.'"]'));
            if($res == 1){
                $out['status'] = true;
                $out['data']['result_id'] = $id;
                $out['data']['cards'] = $this->array2cards($cards);;
                $out['data']['status'] = $status;
                return $out;
                //if($data['A'] >= $data['B']){
/*                 if(true){
                    $res = $db->query()->where(array('parent_id'=>$id))->get();
                    if(empty($res->toArray()) || !isset($res->toArray()[0])){
                        return false;
                    }
                    $data = $res->toArray()[0];
                    return $data['id'];
                } */
            }
        }
        $out['status'] = false;
        return $out;
        
        
    }
    
}