<?php

namespace App\Poker\Repositories;

use Illuminate\Database\Eloquent\Model;

class MappingGame extends Model
{
    //
    protected $table = 'users_mapping_games';
}
