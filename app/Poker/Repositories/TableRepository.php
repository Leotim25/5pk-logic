<?php 
namespace App\Poker\Repositories;


class TableRepository  {
    function __construct(){
        
    }
    public function createNew($room_id)
    {
        $db = new Table();
        $db->room_id = $room_id;
        $db->user_token = "";
        $db->status = 0;
        $db->save();
        return $db->id;
    }
    public function createUpdate($table_id)
    {
        $db = new Table();
        $res = $db->query()->where(array('id'=>$table_id ))->update(
            array(
                'status'=>2 ,
            ));
        if($res == 1){
            return true;
        }
        return false;
    }
    public function userIn($room_id,$table_id,$user_id,$user_token,$user_name,$account_name)
    {
        $db = new Table();
        $res = $db->query()->where(array('user_id'=>$user_id))->get();
        if($res){
            $res = $res->toArray();
            if(!empty($res)){
                foreach ($res as $value){
                    $db->query()->where(array('id'=>$value['id']))->update(
                        array(
                            'status'=>2 ,
                            'user_id'=>NULL,
                            'user_token'=>NULL,
                            'user_name'=>NULL,
                            'account_name'=>NULL,
                        ));
                }
            }
        }
        $res = $db->query()->where(array('id'=>$table_id ,'room_id'=>$room_id ))->whereIn('status', array(2))->update(
            array(
                'status'=>1 ,
                'user_id'=>$user_id,
                'user_token'=>$user_token,
                'user_name'=>$user_name,
                'account_name'=>$account_name,
            ));
        if($res == 1){
            return true;
        }
        return false;
    }
    public function userLeave($room_id,$table_id)
    {
        $db = new Table();
        $res = $db->query()->where(array('id'=>$table_id ,'room_id'=>$room_id))->update(
            array(
                'status'=>2 ,
                'user_id'=>NULL,
                'user_token'=>NULL,
                'user_name'=>NULL,
                'account_name'=>NULL,
            ));
        if($res == 1){
            return true;
        }
        return false;
    }
    public function show($table_id)
    {
        $db = new Table();
        $res = $db->query()->where(array('id'=>$table_id))->get();
        return $res->toArray();
    }
    public function list($room_id = 1)
    {
        $db = new Table();
        $res = $db->query()->where(array('room_id'=>$room_id))->whereIn('status', array(1, 2))->get();
        return $res->toArray();
    }
    public function all($room_id)
    {
        $db = new Table();
        $res = $db->query()->where(array('room_id'=>$room_id))->get();
        return $res->toArray();
    }
    //mappingGame
    public function mappingGame($table_id , $user_id, $user_name,$game_id,$game_type,$level)
    {
        $db = new MappingGame();
        $db->table_id = $table_id;
        $db->user_id = $user_id;
        $db->user_name = $user_name;
        $db->game_id = $game_id;
        $db->game_type = $game_type;
        $db->level = $level;
        $db->save();
        return $db->id;
    }
    public function userPlayShow($table_id){
        //fail
        exit;
        $db = new Table();
        $res = $db->query()->where(array('id'=>$table_id))->get();
        if($res){
            $res = $res->toArray();
            if(count($res) == 1){
                $db = new MappingGame();
                $res = $db->query()->where(array('table_id'=>$table_id))->get();
                if($res){
                    $res = $res->toArray();
                    $out = array();
                    foreach($res as $key=>$value){
                        $out[$key] = array();
                        if($value['game_type'] == "5pk-1"){
                            $repo = new Pk5Repository();
                            $res2 = $repo->show($value['game_id']);
                            if(!$res2){
                                unset($out[$key]); 
                                continue;
                            }
                            $out[$key]['game_id'] = $res2[0]['id'];
                            $out[$key]['game_type'] = $value['game_type'];
                            $out[$key]['table_id'] = $value['table_id'];
                            $out[$key]['level'] = $value['level'];
                            $out[$key]['A'] = $res2[0]['A'];
                            $out[$key]['parent_id'] = $res2[0]['parent_id'];
                            $out[$key]['status'] = $res2[0]['status'];
                            //$out[$key]['child'] = (isset($res2[0]['child']))?$res2[0]['child']:array();
                        }
                        if($value['game_type'] == "5pk-2"){
                            $repo = new DoubleUpRepository();
                            $res2 = $repo->show($value['game_id']);
                            if(!$res2){
                                unset($out[$key]);
                                continue;
                            }
                                $out[$key]['game_id'] = $res2[0]['id'];
                                $out[$key]['game_type'] = $value['game_type'];
                                $out[$key]['table_id'] = $value['table_id'];
                                $out[$key]['level'] = $value['level'];
                                $out[$key]['A'] = $res2[0]['A'];
                                $out[$key]['parent_id'] = $res2[0]['parent_id'];
                                $out[$key]['status'] = $res2[0]['status'];
                        }
                    }
                    return $out;
                }
            }
        }
        //return $res->toArray();
    }
}