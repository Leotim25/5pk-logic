<?php
namespace App\Poker\Repositories;
use App\Poker\PokerRepository;

class Pk5Repository extends PokerRepository {
    /*
     Name	Chinese Name	Rate	代號
     Royal Flush	同花大順	800	10
     Five of a kind	五梅	400	9
     Straight Flush	同花順	100	8
     Four of a kind	鐵支	40	7
     Full House	葫蘆	10	6
     Flush	同花	7	5
     Straight	順子	5	4
     Three of a kind	三條	3	3
     Two Pairs	兩對	2	2
     One Pair	一對(J以上)	1	1*/
    protected $rate = array(
        0=>0,
        1=>1,
        2=>1,
        3=>2,
        4=>3,
        5=>5,
        6=>10,
        7=>20,
        8=>50,
        9=>200,
        10=>800,
        
    );
    protected $judge_name = array(
        0=>'Lose',
        1=>'One Pair',
        2=>'Two Pairs',
        3=>'Three of a kind',
        4=>'Straight',
        5=>'Flush',
        6=>'Full House',
        7=>'Four of a kind',
        8=>'Straight Flush',
        9=>'Five of a kind',
        10=>'Royal Flush',
        
    );
    function __construct(){
        parent::__construct();
        $temp = array();
        foreach ($this->ranks as $v){
            if($v == "A"){
                $temp[10]=$v;
            }else if($v == "B"){
                $temp[11]=$v;
            }else if($v == "D"){
                $temp[12]=$v;
            }else if($v == "E"){
                $temp[13]=$v;
            }else{
                $temp[$v]=$v;
            }
        }
        $temp[99] = "F";
        ksort($temp);
        $this->ranks = $temp;
        $temp2 = array();
        //'S','H','D','C'
        foreach ($this->suit as $v){
            if($v == "A"){
                $temp2[4]=$v;
            }else if($v == "B"){
                $temp2[3]=$v;
            }else if($v == "C"){
                $temp2[2]=$v;
            }else if($v == "D"){
                $temp2[1]=$v;
            }
        }
        $temp2[99] = "D";
        $this->suit = $temp2;
        //5pk加入鬼牌
        $this->nowLeftCards[] = array(99,99);
        shuffle($this->nowLeftCards);
    }
    public function show($id){
        $db = new Pk5();
        $res = $db->query()->where(array('id'=>$id))->get();
        if(empty($res->toArray())){
            return false;
        }
        $data = $res->toArray();
        $res2 = $db->query()->where(array('parent_id'=>$id))->orderBy('id', 'desc')->get();
        if(empty($res2->toArray())){
            $data = $res->toArray();
        }else{
            $data = $res2->toArray();
        }
        return $data;
    }
    
    public function cards2array($cards){
        $out = array();
        foreach($cards as $k=>$data){
            $suit = $data[0];
            $ranks = $data[1];
            if(isset($data[2])){
                $ranks = $data[1].$data[2];
            }
            $out[$k] = array(array_search($ranks, $this->ranks) , array_search($suit, $this->suit));
        }
        return $out;
    }
    public function array2cards($array){
        $out = array();
        foreach($array as $k=>$data){
            $suit = $data[1];
            $ranks = $data[0];
            $out[$k] = $this->suit[$suit].$this->ranks[$ranks];
        }
        return $out;
    }
    public function fixOdds20181112($checkCards){
        $checkCards = $this->cards2array($checkCards);
        $judge = $this->judge($checkCards);
        if($judge == 1 || $judge == 2 || $judge == 3){
            return true;
        }
        return false;
        
    }
    
    
    
    public function randomRank($j=5,$palyers = array('A','B','C','D'))
    {
        shuffle($this->nowLeftCards);
        $out = array();
        $all = $this->array2cards($this->nowLeftCards);
        foreach($palyers as $index => $name){
            for($i=0;$i<$j;$i++){
                $out[$name][] = array_pop($all);
            }
        }
        
        if($this->fixOdds20181112($out['A'])){
            shuffle($this->nowLeftCards);
            $out = array();
            $all = $this->array2cards($this->nowLeftCards);
            foreach($palyers as $index => $name){
                for($i=0;$i<$j;$i++){
                    $out[$name][] = array_pop($all);
                }
            }
        }
        $out['discard'] = $all;
        return $out;
    }
    public function test1($k = array()){
        //ANDY is 帥哥
        $db = new Pk5();
        $start = array();
        $all = $this->array2cards($this->nowLeftCards);
        foreach ($k as $vvv){
            $vvvKey = array_search($vvv, $all);
            unset($all[$vvvKey]);
        }
        $start['A'] = $k;
        foreach($all as $vvvvv){
            $start['discard'][] = $vvvvv;
        }
        $db->player_card = json_encode($start['A']);
        $db->parent_id = 0;
        $db->discard = json_encode($start['discard']);
        $db->save();
        $start['id'] = $db->id;
        return $start;
    }
    public function step1(){
        //ANDY is 帥哥
        $db = new Pk5();
        $start = $this->randomRank(5,array('A'));
        $db->player_card = json_encode($start['A']);
        $db->parent_id = 0;
        $db->discard = json_encode($start['discard']);
        $db->save();
        $start['id'] = $db->id;
        return $start;
    }
    public function step2($room,$use = array()){
        $db = new Pk5();
        $res = $db->query()->where(array('id'=>$room))->get();
        if(empty($res->toArray())){
            return false;
        }else{
            $res2 = $db->query()->where(array('parent_id'=>$room))->orderBy('id', 'desc')->get();
            if(empty($res2->toArray())){
                $all = $res->toArray();
                $j = 5;
                $A = json_decode($all[0]['player_card'],true);
                $start = array();
                $start['A'] = array();
                $start['deal'] = json_decode($all[0]['deal'],true);
                $start['discard'] = json_decode($all[0]['discard'],true);
                
                if(is_array($use)){
                    foreach($use as $u){
                        //do something
                        $start["A"][$u]=$A[$u];
                        unset($A[$u]);
                    }
                }
                //print_R($start);
                //exit;
                
                for($i=0;$i<$j;$i++){
                    if(!isset($start["A"][$i])){
                        $start["A"][$i] = array_pop($start['discard']);
                    }
                }
                foreach($A as $v){
                    $start['deal'][]=$v;
                }
                ksort($start['A']);
                $db->player_card = json_encode($start['A']);
                $db->parent_id = $all[0]['id'];
                $db->deal = json_encode($start['deal']);
                $db->discard = json_encode($start['discard']);
                $db->save();
                $start['id'] = $db->id;
                return $start;
            }else{
                return false;
            }
            
        }
    }
    public function check1($room){
        $update_id = 0;
        $out = array();
        $db = new Pk5();
        $res = $db->query()->where(array('id'=>$room))->get();
        if(empty($res->toArray())){
            return false;
        }
        $data = $res->toArray();
        $res2 = $db->query()->where(array('parent_id'=>$room))->orderBy('id', 'desc')->get();
        if(empty($res2->toArray())){
            $data = $res->toArray();
        }else{
            $data = $res2->toArray();
        }
        $A = $data[0]['player_card'];
        $update_id = $data[0]['id'];
        $discard = $data[0]['discard'];
        $deal = $data[0]['deal'];
        
        $A = json_decode($A,true);
        $discard = json_decode($discard,true);
        $deal = json_decode($deal,true);
        
        $out['A'] = $A;
        $out['discard'] = $discard;
        
        
        $temp = array();
        
        if (false !== $key = array_search("DF", $A)) {
            $wilson = 0;
            unset($A[$key]);
            //鬼牌啟動
            foreach($discard as $vv){
                $temp[$wilson]=$A;
                $temp[$wilson][$key] = $vv;
                $wilson++;
            }
            if(!empty($deal)){
                foreach($deal as $vv22){
                    $temp[$wilson]=$A;
                    $temp[$wilson][$key] = $vv22;
                    $wilson++;
                }
            }
        }else{
            $temp[] = $A;
        }
        $result = array();//最終牌型
        $resultArray = array();//最終牌型
        $judge = 0;
        if(count($temp) == 1){
            $result = $temp[0];
            $resultArray = $this->cards2array($result);
            $judge = $this->judge($resultArray);
            if($judge == 1){
                $exType = $this->exType($resultArray);
                if(!$exType){
                    $judge = 0;
                }
            }
        }else{
            //鬼牌啟動
            //10:皇家同花顺 | 8:同花顺 | 7:四条 | 6:葫芦 | 5:同花 | 4:顺子 | 3:三条 | 2:两对 | 1:一对 | 0:高牌
            $recyc = array();
            $result = array();
            foreach($temp as $kkk=>$vvv){
                $vvv = $this->cards2array($vvv);
                $judge = $this->judge($vvv);
                $recyc[$kkk] = $judge;
                $result[$kkk] = $vvv;
                if($judge == 1){
                    $exType = $this->exType($vvv);
                    if(!$exType){
                        unset($recyc[$kkk]);
                        unset($result[$kkk]);
                    }
                }
            }
            $max  = max($recyc);
            $array_count = array_count_values ($recyc);
            if($max == 7 && $array_count[$max] == 48){
                //五梅
                $result = $out['A'];
                $resultArray = $this->cards2array($result);
                $judge = 9;
            }else if($max == 10){
                $key = array_search($max, $recyc); // $key = 2;
                $result = $temp[$key];
                $resultArray = $this->cards2array($result);
                $judge = 8;
            }else{
                $key = array_search($max, $recyc); // $key = 2;
                $result = $temp[$key];
                $resultArray = $this->cards2array($result);
                $judge = $this->judge($resultArray);
            }
            
        }
        $out['judge'] = $judge;
        $out['result'] = $result;
        $out['resultArray'] = $resultArray;
        $out['judge_name'] = $this->judge_name[$out['judge']];
        $out['rate']= $this->rate[$out['judge']];
        $db->query()->where(array('id'=>$update_id))->update(array('status'=>$out['judge']));
        return $out;
    }
    public function exType($cards){
        $numbers = $this->getCardNumbers($cards);
        foreach ($numbers as $key => $number) {
            if($number == 1){
                $numbers[$key] = 14;
            }
        }
        rsort($numbers);
        $checknum = array(13,14);
        $numbers_count = array_count_values ($numbers);
        foreach($checknum as $num){
            if(isset($numbers_count[$num]) && $numbers_count[$num] ==2){
                return true;
            }
        }
        return false;
    }
    public function checkold($room){
        $update_id = 0;
        $out = array();
        $db = new Pk5();
        $res = $db->query()->where(array('id'=>$room))->get();
        if(empty($res->toArray())){
            return false;
        }
        $data = $res->toArray();
        $res2 = $db->query()->where(array('parent_id'=>$room))->orderBy('id', 'desc')->get();
        if(empty($res2->toArray())){
            $data = $res->toArray();
        }else{
            $data = $res2->toArray();
        }
        $A = $data[0]['A'];
        $update_id = $data[0]['id'];
        $discard = $data[0]['discard'];
        
        $A = json_decode($A,true);
        $discard = json_decode($discard,true);
        
        $out['A'] = $A;
        $out['discard'] = $discard;
        
        
        $temp = array();
        $recyc = array();
        $result = array();
        
        if (false !== $key = array_search("DF", $A)) {
            unset($A[$key]);
            //鬼牌啟動
            foreach($discard as $kk=>$vv){
                $temp[$kk]=$A;
                $temp[$kk][$key] = $vv;
            }
        }else{
            $temp[] = $A;
        }
        foreach($temp as $kkk=>$vvv){
            $vvv = $this->cards2array($vvv);
            $res = $this->judge($vvv);
            $recyc[$kkk] = $res;
            $result[$kkk] = $vvv;
        }
        
        
        $max  = max($recyc);  // 7
        
        $getMaxArray = array();
        
        foreach($recyc as $key=>$value){
            if($value == $max){
                $getMaxArray[] = $key;
            }
        }
        
        $recycCount = array();
        foreach ($recyc as $key=>$value){
            if(isset($recycCount[$value])){
                $recycCount[$value] = $recycCount[$value]+1;
            }else{
                $recycCount[$value] = 1;
            }
        }
        
        $out['judge'] = $max;
        $maxkey = array_search($max, $recyc);
        
        if(isset($recycCount[7]) && $recycCount[7] == 48){
            $out['judge'] = 9;
        }
        if(isset($recycCount[10]) && $recycCount[10] == 1){
            if(isset($recycCount[8]) && $recycCount[8] == 1){
                $out['judge'] = 8;
            }
        }
        
        
        
        if($out['judge'] == 1){
            $twoResult = array();
            foreach ($getMaxArray as $value){
                foreach($result[$value] as $mum){
                    $twoResult[$value][] = $mum[0];
                }
            }
            $useTwoResult = $twoResult;
            
            foreach ($twoResult as $key =>$res){
                $twoCount = array();
                foreach ($res as $res2){
                    if(isset($twoCount[$res2])){
                        $twoCount[$res2] = $twoCount[$res2]+1;
                    }else{
                        $twoCount[$res2] = 1;
                    }
                }
                foreach($twoCount as $ggg => $gggvvv){
                    if($gggvvv == 2 && ($ggg == 11 || $ggg == 12 || $ggg == 13 || $ggg == 1)){
                        
                    }else{
                        unset($twoCount[$ggg]);
                    }
                }
                if(empty($twoCount)){
                    unset($twoResult[$key]);
                }
            }
            if(empty($twoResult)){
                $out['judge'] = 0;
            }
            $twoMax  = max($useTwoResult);  // 7
            $twoMaxkey = array_search($twoMax, $useTwoResult);
            $maxkey = $twoMaxkey;
        }
        $db->query()->where(array('id'=>$update_id))->update(array('status'=>$out['judge']));
        $out['judge_name'] = $this->judge_name[$out['judge']];
        $out['result'] = $temp[$maxkey];
        $out['resultArray'] = $this->cards2array($temp[$maxkey]);
        $out['rate']= $this->rate[$out['judge']];
        return $out;
    }
}