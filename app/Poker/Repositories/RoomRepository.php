<?php 
namespace App\Poker\Repositories;

class RoomRepository  {
    function __construct(){
        
    }
    public function list(){
        $db = new Room();
        $res = $db->query()->get();
        return $res->toArray();
    }
    public function show($room_id = 1){
        $db = new Room();
        $res = $db->query()->where(array('id'=>$room_id))->get();
        return $res->toArray();
    }
}