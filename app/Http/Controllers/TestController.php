<?php 
namespace App\Http\Controllers;

use App\Poker\Repositories\Pk5Repository;
use App\Poker\Repositories\DoubleUpRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class TestController extends Controller
{
    /**
     */
    protected $pk5;
    protected $doubleup;
    
    /**
     * TestController constructor.
     * @param $posts
     */
    public function __construct(Pk5Repository $pk5 , DoubleUpRepository $doubleup)
    {
        $this->pk5 = $pk5;
        $this->doubleup = $doubleup;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function step1(Request $request)
    // {
    //     $all = $request->all();
    //     //print_R($all);
    //     if(isset($all['A'])){
    //         $all['A'] = json_decode($all['A'],true);
    //     }else{
    //         $temp = array();
    //         $temp[]='["A1","B1","C1","D1","DF"]';
    //         $temp[]='["A1","B1","C1","D1","B2"]';
    //         $temp[]='["A1","AA","AB","AD","AE"]';
    //         $temp[]='["A2","A3","A4","A5","A6"]';
    //         shuffle($temp);
    //         $all['A'] = $temp[0];
    //         $all['A'] = json_decode($all['A'],true);
    //     }
    //     $out = $this->pk5->test1($all['A']);
    //     return Response::json(array(
    //         'status'      =>  true,
    //         'data'   =>  $out
    //     ), 200);
    //     return Response::json(array(
    //         'status'      =>  false
    //     ), 500);
    // }
    public function check1(Request $request)
    {
        $all = $request->all();
        if(isset($all['id']) && intval($all['id']) > 0 ){
            $res = $this->pk5->check1($all['id']);
            if($res != false){
                return Response::json(array(
                    'status'      =>  true,
                    'data'   =>  $res
                ), 200);
            }
        }
        return Response::json(array(
            'status'      =>  false
        ), 500);
    }
}