<?php 
namespace App\Http\Controllers;

use App\Poker\Repositories\Pk5Repository;
use App\Poker\Repositories\DoubleUpRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class Pk5Controller extends Controller
{
    /**
     */
    protected $pk5;
    protected $doubleup;
    
    /**
     * TestController constructor.
     * @param $posts
     */
    public function __construct(Pk5Repository $pk5 , DoubleUpRepository $doubleup)
    {
        $this->pk5 = $pk5;
        $this->doubleup = $doubleup;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function step1(Request $request)
    {
        $all = $request->all();
        if(isset($all['id']) && intval($all['id']) > 0 ){
            $room = $all['id'];
            if(isset($all['A'])){
                $positionsA = explode(",", $all['A']);
            }else{
                $positionsA = array();
            }
            if(is_array($positionsA)){
                foreach ($positionsA as $kkkk => $vvvvv){
                    $vvvvv = intval($vvvvv);
                    if($vvvvv >= 1){
                        $vvvvv = $vvvvv-1;
                        $positionsA[$kkkk] = $vvvvv;
                    }else{
                        unset($positionsA[$kkkk]);
                    }
                }
                $out = $this->pk5->step2($room,$positionsA);
                if($out == false){
                    return Response::json(array(
                        'status'      =>  false
                    ), 500);
                }
                return Response::json(array(
                    'status'      =>  true,
                    'data'   =>  $out
                ), 200);
            }
        }
        $out = $this->pk5->step1();
        return Response::json(array(
            'status'      =>  true,
            'data'   =>  $out
        ), 200);
    }
    public function check1(Request $request)
    {
        $all = $request->all();
        if(isset($all['id']) && intval($all['id']) > 0 ){
            $res = $this->pk5->check1($all['id']);
            if($res != false){
                return Response::json(array(
                    'status'      =>  true,
                    'data'   =>  $res
                ), 200);
            }
        }
        return Response::json(array(
            'status'      =>  false
        ), 500);
    }
    public function step2(Request $request)
    {
        $all = $request->all();
        if(isset($all['id']) && intval($all['id']) > 0 ){
            $out = $this->doubleup->step1($all['id']);
        }else{
            $out = $this->doubleup->step1();
        }
        return Response::json(array(
            'status'      =>  true,
            'data'   =>  $out
        ), 200);
        //return false;
    }
    public function check2(Request $request)
    {
        $all = $request->all();
        if(isset($all['id']) && intval($all['id']) > 0 ){
            if(isset($all['site']) && intval($all['site'])>=0){
                $res = $this->doubleup->step2($all['id'],$all['site']);
                if(isset($res['status']) && $res['status'] == true){
                    return Response::json(array(
                        'status'      =>  true,
                        'data'   =>  $res['data']
                    ), 200);
                }
            }
        }
        return Response::json(array(
            'status'      =>  false
        ), 500);
    }
}