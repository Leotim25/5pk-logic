<?php

namespace App\Http\Controllers;

use App\Poker\Repositories\TableRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
/*
 * 
 Verb	URI	Action	Route Name
GET	/photos	index	photos.index
GET	/photos/create	create	photos.create
POST	/photos	store	photos.store
GET	/photos/{photo}	show	photos.show
GET	/photos/{photo}/edit	edit	photos.edit
PUT/PATCH	/photos/{photo}	update	photos.update
DELETE	/photos/{photo}	destroy	photos.destroy
 * 
 * 
 * 
 * 
 */
class TableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $tableRepository;
    
    
    function __construct(TableRepository $tableRepository){
        $this->tableRepository = $tableRepository;
    }
    
    
    public function index()
    {
        //list
        return $this->tableRepository->list();
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request){
        $all = $request->all();
        if(isset($all['room_id']) && isset($all['table_id'])){
            $room_id = intval($all['room_id']);
            $table_id = intval($all['table_id']);
            $res = $this->tableRepository->userLeave($room_id ,$table_id);
            if($res){
                return Response::json(array(
                    'status'      =>  true,
                ), 200);
            }
        }
        return Response::json(array(
            'status'      =>  false
        ), 500);
    }
    public function store(Request $request)
    {
        /*
                        'user_id'=>$user_id,
                'user_token'=>$user_token,
                'user_name'=>$user_name,
                'account_name'=>$account_name,
        */
        $all = $request->all();
        if(isset($all['room_id']) && isset($all['table_id']) && isset($all['user_token']) && $all['user_token']!=""){
            $room_id = intval($all['room_id']);
            $table_id = intval($all['table_id']);
            $user_id = $all['user_id'];
            $user_token = $all['user_token'];
            $user_name = $all['user_name'];
            $account_name = $all['account_name'];
            $res = $this->tableRepository->userIn($room_id ,$table_id, $user_id,$user_token,$user_name,$account_name);
            if($res){
                return Response::json(array(
                    'status'      =>  true,
                ), 200);
            }
        }
        return Response::json(array(
            'status'      =>  false
        ), 500);
    }
    public function mappingGame(Request $request)
    {
        //
        //
        $all = $request->all();
        if(isset($all['user_id']) && isset($all['game_id']) && isset($all['game_type']) && isset($all['level']) && isset($all['table_id'])){
            $table_id = intval($all['table_id']);
            $user_id = intval($all['user_id']);
            $game_id = intval($all['game_id']);
            $user_name = (isset($all['user_name']))? $all['user_name']: "";
            $game_type = (isset($all['game_type']))? $all['game_type']: "";
            $level = intval($all['level']);
            $res = $this->tableRepository->mappingGame($table_id ,$user_id, $user_name,$game_id,$game_type,$level);
            if($res > 0 ){
                return Response::json(array(
                    'status'      =>  true,
                ), 200);
            }
            
        }
        return Response::json(array(
            'status'      =>  false
        ), 500);
    }
}
