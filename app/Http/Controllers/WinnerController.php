<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
/*
 * 
 Verb	URI	Action	Route Name
GET	/photos	index	photos.index
GET	/photos/create	create	photos.create
POST	/photos	store	photos.store
GET	/photos/{photo}	show	photos.show
GET	/photos/{photo}/edit	edit	photos.edit
PUT/PATCH	/photos/{photo}	update	photos.update
DELETE	/photos/{photo}	destroy	photos.destroy
 * 
 * 
 * 
 * 
 */
class WinnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $tableRepository;
    
    
    function __construct(){
        
    }
    
    
    public function mappingGame(Request $request)
    {   //bet_amount
        //win_amount
        $all = $request->all();
        if(isset($all['game_id']) && isset($all['bet_amount']) && isset($all['win_amount'])){
            $input = array(
                'table_id'=>0,
                'user_id' =>0,
                'user_name' =>'',
                'game_id' =>$all['game_id'],
                'game_status' =>0,
                'bet_amount' =>$all['bet_amount'],
                'win_amount' =>$all['win_amount'],
            );
            DB::table('result_winner')->insert($input);
            return Response::json(array(
                'status'      =>  true,
            ), 200);
        }
        return Response::json(array(
            'status'      =>  false
        ), 500);
    }
}
