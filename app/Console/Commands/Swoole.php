<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;

class Swoole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $timer;
    protected $count = 30;
    protected $signature = 'swoole {action?}';
    //protected $signature = 'swoole:send {user}';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'swoole';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $action = $this->argument('action');
        switch ($action) {
            case 'close':
                
                break;
            case 'start':
                $this->start();
                break;
            case 'go':
                $this->go2();
                break;
            default:
                break;
        }
        
    }
    public function discount(){
        sleep(1);
        return $this->count = $this->count-1;
    }
    public function go2(){
        $workers = [];
        $worker_num = 1;
        for($i = 0; $i < $worker_num; $i++)
        {
            $process = new \Swoole\Process(function($worker){
                //接受来自主进程的消息
                $recv = $worker->pop();
                echo "From Master: $recv\n";
                $worker->exit(0);
            },false,false);
            $process->useQueue();
            $pid = $process->start();
            $workers[$pid] = $process;
        }
        foreach($workers as $pid => $process)
        {
            //给子进程发布消息
            $process->push("hello worker[$pid]\n");
        }
        //等待消息停止
        for($i = 0; $i < $worker_num; $i++)
        {
            $ret = \Swoole\Process::wait();
            $pid = $ret['pid'];
            //unset($workers[$pid]);
            //echo "Worker Exit, PID=".$pid.PHP_EOL;
        }
    }
    public function start(){
        $process = new \Swoole\Process(function($worker){
            //接受来自主进程的消息
            //echo 1;
        },false,true);
        $process->start();
        $ws = new \swoole_websocket_server("0.0.0.0", 9502);
        //监听WebSocket连接打开事件
        $ws->on('open', function ($ws, $request) {
            $fd = $request->fd;
            $this->timer = swoole_timer_tick(1000,function() use ($fd,$ws){
                $ws->push($fd, "after ".$this->count."s.\n");
                $this->count = $this->count-1;
                if($this->count <= 0 ){
                    swoole_timer_clear($this->timer);
                }
            });
                $ws->push($fd, "hello, welcome\n");
        });
            
            //监听WebSocket消息事件
            $ws->on('message', function ($ws, $frame) {
                //$this->status = false;
                echo "Message: {$frame->data}\n";
                echo $this->timer;
                //echo $this->goTag;
                if($this->timer){
                    swoole_timer_clear($this->timer);
                    $this->timer = 0;
                    $this->count =30;
                }
                $ws->push($frame->fd, "server: {$frame->data}");
            });
                
                //监听WebSocket连接关闭事件
                $ws->on('close', function ($ws, $fd) {
                    echo "client-{$fd} is closed\n";
                });
                    
                    $ws->start();
    }
    public function start2()
    {
        
        //创建websocket服务器对象，监听0.0.0.0:9502端口
        $ws = new \swoole_websocket_server("0.0.0.0", 9502);
        
        //监听WebSocket连接打开事件
        $ws->on('open', function ($ws, $request) {
            // var_dump($request->fd, $request->get, $request->server);
            //echo $this->count();
            $ws->push($request->fd, "hello, welcome\n");
            //echo $this->count();
            //exit;
            //$process = new \Swoole\Process('swooleCallback');
            //callback_function();
            //$process = new \Swoole\Process('callback_function');
            
            //$pid = $process->start();
            //echo $pid;
            
            while ($this->status) {
                $discount = $this->discount();
                if($this->status == false){
                    break;
                }
                if($discount <= 0){
                    break;
                }
                $ws->push($request->fd, $discount);
            }
        });
            //监听WebSocket消息事件
            $ws->on('message', function ($ws, $frame) {
                $this->status = false;
                var_dump($this->status);
                echo "\n";
                echo "Message: {$frame->data}\n";
                $ws->push($frame->fd, "server: {$frame->data}");
            });
                
                //监听WebSocket连接关闭事件
                $ws->on('close', function ($ws, $fd) {
                    echo "client-{$fd} is closed\n";
                });
                    
                    $ws->start();
    }
}