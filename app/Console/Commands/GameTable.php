<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Poker\Repositories\TableRepository;
use App\Poker\Repositories\RoomRepository;

class GameTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'table:create {room_id}';
    //protected $signature = 'swoole:send {user}';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '建立一房幾間';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $room_id = $this->argument('room_id');
        $room = new RoomRepository;
        $table = new TableRepository;
        $res = $room->show($room_id);
        if(!empty($res)){
            $max = $res[0]['upper'];
            $use_max = $res[0]['upper'];
            $list = $table->all($room_id);
            foreach ($list as $value){
                if($value['status'] == 1){
                    $max = $max-1;
                }
                if($value['status'] == 0){
                    $max = $max-1;
                }
            }
            if($max > 0){
                for($i=1;$i<=$max;$i++){
                    $table->createNew($room_id);
                }
            }
            $list = $table->all($room_id);
            foreach ($list as $value){
                if($value['status'] == 1){
                    $use_max = $use_max-1;
                }else{
                    if($use_max>0){
                        $table_id = $value['id'];
                        $table->createUpdate($table_id);
                    }
                }
            }
        }
        //$table->createNew($room_id);
    }
}