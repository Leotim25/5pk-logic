<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/5pk/sp1', [
    'uses' => 'Pk5Controller@step1'
]);
Route::post('/5pk/sp1', [
    'uses' => 'Pk5Controller@check1'
]);
Route::get('/5pk/sp2', [
    'uses' => 'Pk5Controller@step2'
]);
Route::post('/5pk/sp2', [
    'uses' => 'Pk5Controller@check2'
]);

Route::get('/table/list', [
    'uses' => 'TableController@index'
]);
Route::post('/table/create', [
    'uses' => 'TableController@store'
]);
Route::post('/table/leave', [
    'uses' => 'TableController@destroy'
]);
Route::post('/table/mappingGame', [
    'uses' => 'TableController@mappingGame'
]);

Route::get('/winner/mappingGame', [
    'uses' => 'WinnerController@mappingGame'
]);


Route::get('/room/test', [
    'uses' => 'RoomController@test'
]);



Route::get('/test/sp1', [
    'uses' => 'TestController@step1'
]);
Route::post('/test/sp1', [
    'uses' => 'TestController@check1'
]);
